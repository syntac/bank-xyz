@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <table class="table table-bordered bg-white shadow">
        <thead class="thead-light">
            <tr>
                <th>Invoice Number</th>
                <th>Account Code</th>
                <th>Account Name</th>
                <th>Account Number</th>
                <th>Payer Bank</th>
                <th>Payer Number</th>
                <th>Amount</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($data as $item)
                <tr>
                    <td>{{ $item->invoice_number }}</td>
                    <td>{{ $item->account_code }}</td>
                    <td>{{ $item->account_name }}</td>
                    <td>{{ $item->account_number }}</td>
                    <td>{{ $item->payer_bank }}</td>
                    <td>{{ $item->payer_number }}</td>
                    <td>Rp. {{ num2rp($item->amount) }}</td>
                </tr>
            @empty
            <tr>
                <td colspan="7">
                    <p class="alert alert-info text-center">No data available yet.</p>
                </td>
            </tr>
            @endforelse
        </tbody>
    </table>

    {{ $data->links() }}
</div>

@endsection
