@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <table class="table unpaid-table table-bordered bg-white shadow">
        <thead class="thead-light">
            <tr>
                <th>Student</th>
                <th>Invoice Number</th>
                <th>Amount</th>
                <th>Account Code</th>
                <th>Account Name</th>
                <th>Account Number</th>
                <th>Type</th>
                <th>Status</th>
                <th class="text-right">Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr class="loading-data">
                <td colspan="9">
                    <p class="alert alert-info text-center">Please wait, fetching data from Campus Server.</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>

<!-- Modal -->
<div class="modal fade" id="PaymentModal" tabindex="-1" role="dialog" aria-labelledby="PaymentModalTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Add Payment to Invoice <span class="number-txt"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
          <div class="modal-body">
            <div class="alert alert-info text-center">
                <span class="h4">The amount to be paid is <span class="font-weight-bold amount-txt"></span></span>
            </div>
            <div class="loading text-center" style="display:none">
                <div class="spinner-border text-primary" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
                <p>Please wait...</p>
            </div>
            <form id="form-payment">
                <div class="form-group">
                    <label for="payer_bank">Payer Bank</label>
                    <select name="payer_bank" class="form-control" id="payer_bank">
                      <option value="BRI">BRI</option>
                      <option value="BCA">BCA</option>
                      <option value="BNI">BNI</option>
                      <option value="Bank Mandiri">Bank Mandiri</option>
                      <option value="MayBank">MayBank</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="payer_number">Payer Account Number</label>
                    <input type="text" name="payer_number" id="payer_number" class="form-control" placeholder="Payer Bank Account Number"></input>
                </div>
                <div class="form-group">
                    <label for="amount">Amount</label>
                    <input type="number" name="amount" id="amount" class="form-control" placeholder="Payment Amount"></input>
                </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary submit">Add Payment</button>
          </div>
        </div>
    </div>
</div>

@endsection
