@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <table class="table paid-table table-bordered bg-white shadow">
        <thead class="thead-light">
            <tr>
                <th>Student</th>
                <th>Invoice Number</th>
                <th>Amount</th>
                <th>Account Code</th>
                <th>Account Name</th>
                <th>Account Number</th>
                <th>Type</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <tr class="loading-data">
                <td colspan="9">
                    <p class="alert alert-info text-center">Please wait, fetching data from Campus Server.</p>
                </td>
            </tr>
        </tbody>
    </table>
</div>

@endsection
