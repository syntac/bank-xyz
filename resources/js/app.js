require('./bootstrap');

const CSRF = $('meta[name=csrf-token]').attr("content");
const HOST = $('meta[name=host]').attr("content");

// Fungsi formatRupiah
function num2rp(angka, prefix){
	var number_string = angka.replace(/[^,\d]/g, '').toString(),
	split   		= number_string.split(','),
	sisa     		= split[0].length % 3,
	rupiah     		= split[0].substr(0, sisa),
	ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

	// tambahkan titik jika yang di input sudah menjadi angka ribuan
	if(ribuan){
		separator = sisa ? '.' : '';
		rupiah += separator + ribuan.join('.');
	}

	rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
	return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

// ucfirst
function ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/* load unpaid data */
function pushUnpaid(item){
	const $template = '<tr id="'+ item.number +'">'+
			'<td>'+ item.student.name +'</td>'+
			'<td>'+ item.number +'</td>'+
			'<td>Rp. '+ num2rp(item.amount) +'</td>'+
			'<td>'+ item.account.code +'</td>'+
			'<td>'+ item.account.name +'</td>'+
			'<td>'+ item.account.number +'</td>'+
			'<td>'+ ucfirst(item.account.status) +'</td>'+
			'<td><span class="badge badge-danger">'+ ucfirst(item.status) +'</span></td>'+
			'<td class="text-right"><button type="button" data-number="'+ item.number +'" class="btn btn-sm btn-primary add-payment">Add Payment</button></td>'+
		'</tr>';
	$('tbody').append($template);
}

if ($('.unpaid-table')[0]){
	$.ajax({
		url: HOST+'/api/invoices/unpaid',
		type: 'GET',
		dataType: 'JSON',
	})
	.done(function($data) {

        if($data.length > 0){
            $('.loading-data').hide();

            $.each($data, function(index, item) {
                pushUnpaid(item);
            });
        } else {
            $('.loading-data .alert-info').text('There is no unpaid invoice right now.')
        }

	})
	.fail(function() {
		alert('Failed to load data from Campus server, please contact the Campus Administrator.');
	});
	
}

/* end unpaid data */

/* load unpaid data */
function pushPaid(item){
    const $template = '<tr id="'+ item.number +'">'+
            '<td>'+ item.student.name +'</td>'+
            '<td>'+ item.number +'</td>'+
            '<td>Rp. '+ num2rp(item.amount) +'</td>'+
            '<td>'+ item.account.code +'</td>'+
            '<td>'+ item.account.name +'</td>'+
            '<td>'+ item.account.number +'</td>'+
            '<td>'+ ucfirst(item.account.status) +'</td>'+
            '<td><span class="badge badge-success">'+ ucfirst(item.status) +'</span></td>'+
        '</tr>';
    $('tbody').append($template);
}

if ($('.paid-table')[0]){
    $.ajax({
        url: HOST+'/api/invoices/paid',
        type: 'GET',
        dataType: 'JSON',
    })
    .done(function($data) {

        if($data.length > 0){
            $('.loading-data').hide();

            $.each($data, function(index, item) {
                pushPaid(item);
            });
        } else {
            $('.loading-data .alert-info').text('There is no unpaid invoice right now.')
        }

    })
    .fail(function() {
        alert('Failed to load data from Campus server, please contact the Campus Administrator.');
    });
    
}

/* end unpaid data */

/* add payment */
$(document).on('click', '.add-payment', function(){ 
    const invoice = $(this).data('number');
    const $modal = $('#PaymentModal');

    $.ajax({
    	url: HOST + '/api/invoice/'+invoice,
    	type: 'GET',
    	dataType: 'json',
    })
    .done(function($data) {
    	console.log($data[0]);

    	$modal.find('.number-txt').text($data[0].number);
    	$modal.find('.amount-txt').text('Rp. '+num2rp($data[0].amount));
    	$modal.find('#amount').val($data[0].amount);

    	if($data[0].account.status == 'fix'){
    		$modal.find('#amount').prop('readonly', true);
    	} else {
    		$modal.find('#amount').prop('readonly', false);
    	}

    	$modal.modal('toggle');

    	$modal.find('.submit').click(function(e) {
    		e.preventDefault();

    		$modal.find('.loading').fadeIn();
    		$modal.find('#form-payment').fadeOut();

    		$.ajax({
    			url: '/payment',
    			type: 'POST',
    			dataType: 'json',
    			data: {
    				_token: CSRF,
    				'invoice_number': invoice,
    				'account_code': $data[0].account.code,
    				'account_number': $data[0].account.number,
    				'account_name': $data[0].account.name,
    				'payer_bank': $modal.find('#payer_bank').find(':selected').val(),
    				'payer_number': $modal.find('#payer_number').val(),
    				'amount': $modal.find('#amount').val(),
    			},
    		})
    		.done(function($data) {
    			if($data.errors){
    				$.each($data.errors, function(field, msg) {
    					alert(msg);
    				});
    			} else {
    				alert('Payment added to invoice '+invoice);
    				window.location.reload();
    			}
    		})
    		.fail(function() {
    			alert('System error, please try again later.')
    		})
    		.always(function() {
    			$modal.find('.loading').hide();
    			$modal.find('#form-payment').fadeIn();
    		});
    		
    	});
    })
    .fail(function() {
    	alert('System error, please try again later.')
    });
    
});