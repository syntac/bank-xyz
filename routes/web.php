<?php
Auth::routes(['register' => false]);

Route::group(['middleware' => 'auth'], function () {
	Route::get('/', 'AppController@index');
	Route::get('/home', 'AppController@index')->name('home');
	Route::get('/paid', 'AppController@paid')->name('paid');
	Route::get('/transactions', 'AppController@transactions')->name('transactions');
	Route::post('/payment', 'AppController@payment')->name('payment');
});
