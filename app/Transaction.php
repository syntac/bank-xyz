<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * Remove guarded fields
     */
    protected $guarded = [];
}
