<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Transaction;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Carbon\Carbon;

class AppController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function paid()
    {
        return view('paid');
    }

    public function transactions()
    {
        $data = Transaction::orderBy('created_at', 'DESC')->paginate(10);

        return view('transactions', compact('data'));
    }

    /**
     * Made a payment
     */
    public function payment(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'payer_bank' => 'required|string|not_in:0',
            'payer_number' => 'required|string|max:225',
            'amount' => 'required|integer|min:1',
        ]);

        if($validator->fails())
        {
            $response = [ 'status' => 'error', 'errors' => $validator->errors() ];            
        } else {
            $save = Transaction::create([
                'invoice_number' => $request->invoice_number,
                'account_code' => $request->account_code,
                'account_number' => $request->account_number,
                'account_name' => $request->account_name,
                'payer_bank' => $request->payer_bank,
                'payer_number' => $request->payer_number,
                'amount' => $request->amount
            ]);

            if($save){
                $client = new Client();
                $result = $client->post(env('HOST').'api/payment/'.$request->invoice_number);

                $response = [ 'status' => 'success' ];
            } else {
                $response = [ 'status' => 'error', 'errors' => $validator->errors() ];
            }
        }

        return response()->json($response);
    }
}
